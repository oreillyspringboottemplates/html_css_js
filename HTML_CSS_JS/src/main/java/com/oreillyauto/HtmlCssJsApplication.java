package com.oreillyauto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HtmlCssJsApplication {

	public static void main(String[] args) {
		SpringApplication.run(HtmlCssJsApplication.class, args);
	}

}
